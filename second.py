import numpy as np

def Phi0(line):
    #This instruction check the value of each index and when 0 became 1 (=entry point in the grain), it record the coordinate.
    #line is the selected entering line and it's use in the case where Phi = 0 in the main program
    
    for Entry in line:
        Entry = 0
        Init = line[Entry]
        while Init != 1:
            Entry += 1
            Init = line[Entry] if Entry != len(line) else 1  
    return Entry

def Posientry(grain, line):
    #Find the first '1' (= entry point) by converting the list 'line' obtained with get_line (see below) and memorize the coordinate of the position
    #'grain' is the loaded data and 'line' the path traveled by the photon
    
    i = 0
    try:
        while grain[line[i]] != 1:
            i += 1
        return line[i]
    except IndexError:
        return



def ToPixel(SizeTotal,Sizepixel):
    #This function convert cm to pixel
    return int(SizeTotal/Sizepixel)


def Carbon(Pi,Sizegrain):
    #Give the number of carbon atoms Nc in a spherical grain with 2.23 the graphit density take on : http://mineraux.uqac.ca/mineraux_detail/graphite.htm
    #And 12.01 the carbon's mass with 1.67E-27 the mass of a proton/neutron
    return ((4/3)*Pi*(Sizegrain/2)**3*2.23)/(12.01*1.67E-27)


def IoniPot(Nc):
    #Classical expression for the ionization potential of a circular grain
    Z = 6                                       #Z for the carbon's atom
    Nc = Nc
    return 4.4 + (Z + 0.5)*(25.1/Nc**0.5)

def get_line(start, end):
    """Already made algorithm find at http://www.roguebasin.com/index.php?title=Bresenham%27s_Line_Algorithm#Python
    Bresenham's Line Algorithm
    Produces a list of tuples from start and end
    
    To test it:
        points1 = get_line((3,2), (-5,-5))
        print(points1)
        >>> [(3, 2), (2, 1), (1, 0), (0, -1), (-1, -2), (-2, -2), (-3, -3), (-4, -4), (-5, -5)]

    """
    # Setup initial conditions
    x1, y1 = start
    x2, y2 = end
    dx = x2 - x1
    dy = y2 - y1
 
    # Determine how steep the line is
    is_steep = abs(dy) > abs(dx)
 
    # Rotate line
    if is_steep:
        x1, y1 = y1, x1
        x2, y2 = y2, x2
 
    # Swap start and end points if necessary and store swap state
    swapped = False
    if x1 > x2:
        x1, x2 = x2, x1
        y1, y2 = y2, y1
        swapped = True
 
    # Recalculate differentials
    dx = x2 - x1
    dy = y2 - y1
 
    # Calculate error
    error = int(dx / 2.0)
    ystep = 1 if y1 < y2 else -1
 
    # Iterate over bounding box generating points between start and end
    y = y1
    points = []
    for x in range(x1, x2 + 1):
        coord = (y, x) if is_steep else (x, y)
        points.append(coord)
        error -= abs(dy)
        if error < 0:
            y += ystep
            error += dx
 
    # Reverse the list if the coordinates were swapped
    if swapped:
        points.reverse()
    return points

def dot(grain,distance,parameter,angle,Entry,Pixel):
    #Find the last value of the aiming line
    #grain is the loaded data, distance = da or de, parameter = la or le, angle = phi or theta, Entry is the entry point computing and Pixel the size of a pixel
    
    distance = np.random.exponential(parameter)                 #Computation of the escape distance of the electron in the grain in cm with the parameter

     
    x = distance*np.cos(angle)                                  #Computation of r*cos(angle)
    y = distance*np.sin(angle)                                  #Computation of r*sin(angle)
    a = Entry[1] + ToPixel(x,Pixel)                             #x axis in pixel of where he have been ejected
    b = Entry[0] + ToPixel(y,Pixel)                             #y axis in pixel of where he have been ejected     
               
    try:                                                        #Check the last value of the list, if it's '0', the photon or electron is out of the grain
        points1 = get_line((Entry[0],Entry[1]), (b,a))
        if grain[points1[-1]] == 1:
            return Aimline(grain,points1,distance,Pixel,Entry,a,b,angle)
               
    except 0 or IndexError:
        return

def Aimline(grain,point,lenght,Pixel,Entry,a,b,angle):
    #This module will add and read all value of 0 and 1 found by get_line in a list to see if the distance of absorption/ejection have to be modify,
    #for example if the electron go out of the grain for 2 pixels, we need to not take this distance into consideration
    #grain is the loaded data, point = get_line of the aiming line, lenght = da or de, Pixel = size of a pixel, Entry = entry point of the photon,
    #a and b parameter computing in dot definition and angle = phi or theta
    
    Aim = []                                                 #Variable which will contain all the value encounter
    d = np.floor(ToPixel(lenght,Pixel))                      #Attribute the value of the distance initialy traveled by the photon/electron 
    i = 1
    
    while i < len(point):
        Aim.append(grain[point[i-1]])
        
        if Aim[i-1] == 1:
            d -= 1
        else:
            a += int(np.around(np.cos(angle)))
            b += int(np.around(np.sin(angle)))
            point = get_line((Entry[0],Entry[1]),(b,a))
            Aim.append(grain[point[-1]])

        i += 1
    
    return point[-1]