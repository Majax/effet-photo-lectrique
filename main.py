# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 09:20:17 2019

@author: Dylan Chosson
Ending : Thurs Oct 03 14:05:26 2019
"""
#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from second import *


with open("donnees_photoelectrons", 'w', encoding='utf-8') as value:

    grain = np.loadtxt('Grain_N150_S1p0_B3p0.txt')                  #Load the grain formed with GenGrain
    MatrixSize = 150                                                #The size N of the matrix decided in GenGrain
    la = 1E-6                                                       #Photon attenuation lenght in cm!
    le = 1E-7                                                       #Electron escape lenght in cm!
    Sizegrain = 2*np.random.uniform(3.1E-8,1E-6)                    #Random value of the diameter of the grain in cm between min and max value of grain radius

    for i in range(20000):
        #All constant or computing constant needed

        Number = np.random.randint(len(grain[:,0]))                 #Choose a random number for the initial line position 

        Energy = np.random.randint(3,16)                            #Photon's energy in a white specter between 3 and 15 eV

        TotalPixel = np.max(np.sum(grain, axis = 1))                #Size (diameter) of the grain in pixels
        Pixel = Sizegrain/TotalPixel                                #Size of a pixel

        da = np.random.exponential(la)                              #Computation of the distance traveled by the Photon in the grain in cm with the parameter la
        
        phi = np.random.uniform(-np.pi/2, np.pi/2)                  #Angle of the coming photon for the grain
        
        #_________________________________________________________________________________________________________________________________________________________
        #Photon's direction depending on angle result
        
        if phi > 0:
            column = int(np.floor(Number*np.tan(phi)))              #Using the property that tan(angle) = Opposite / Adjacent side to find the last column
            Line = get_line((Number,0), (0,column))                 #Take every element of the aiming line
            Entry = Posientry(grain, Line)                          #Look for the first '1' in Line, if it exist, it's the Entry of the photon in the grain
            if Entry == None:
                Verif = 0
            else: 
                Verif = 1     
                Absorb = dot(grain,da,la,-phi,Entry,Pixel)          #Computing of the absorption dot
            
        elif phi == 0:
            column = Phi0(grain[Number,:])
            Entry = Posientry(grain, Line)
            if Entry == None:
                Verif = 0
            else:
                Verif = 1
                Absorb = (Entry[0],Entry[0] + ToPixel(da,Pixel))      
                #The coordinate of Absorb is the line chosen with Number and : Entering column + the distance traveled convert in Pixel
            
        else:
            column = int(np.floor((MatrixSize-Number)/(-np.tan(phi))))  #Same computation as phi > 0 but for phi < 0
            Line = get_line((Number,0), (MatrixSize,column))                 
            Entry = Posientry(grain, Line)
            if Entry == None:
                Verif = 0
            else:
                Verif = 1
                Absorb = dot(grain,da,la,-phi,Entry,Pixel)
        

        #_________________________________________________________________________________________________________________________________________________________
        


        if Verif == 1:                                                      #Verif is a variable which verify if the grain entered the grain, '1' = yes, '0' = no 
            
            Ejection = 0.5*(1+np.tanh((Energy-8)/2))                        #Probability for an electron to be ejected with E0 = 8 eV
            
            if Ejection >= 0.5:                                             #We choose value > 0.5 the value in which the grain is ejected
                
                value.write("{}\n".format(-3))                              #As asking if we have absorption with photo-eletric phenomena, we write '-3'
                theta = np.random.uniform(0, 2*np.pi)                       #Angle of ejection for the electron in a circle of radius de
                de = np.random.exponential(le)                              #Computation of the escape distance of the electron in the grain in cm with the parameter le
                
                Tempo = dot(grain,de,le,theta,Entry,Pixel)
                
                Nc = Carbon(np.pi,Sizegrain)                                #To have the number of Carbon in the grain
                Ee = Energy - np.around(IoniPot(Nc), decimals=1)            #Computation of the kinetic energy of the ejected electron
                value.write("{}\n".format(Ee))                              #Write the value of the kinetic energy
                
            else:
                value.write("{}\n".format(-2))                              #As asking if we have absorption without photo-eletric phenomena, we write '-2'
        else:
            value.write("{}\n".format(-1))                                  #As asking if the photon is not absorbed, we write '-1'

value.close()

Result = np.loadtxt("donnees_photoelectrons")                               #Table [value1 value2 value3 etc] with data from the loading txt                                                

plt.figure(1)
one = len(list(filter(lambda a : a == -1, Result)))                         #A variable in which we will count the number of -1
two = len(list(filter(lambda a : a == -2, Result)))                         #A variable in which we will count the number of -2
three = len(list(filter(lambda a : a == -3, Result)))                       #A variable in which we will count the number of -3

names = ['-1 value', '-2 value', '-3 value']                                #x_axis of the bar plot
Values = [one, two, three] 
plt.bar(names,Values)                                                       #Plot a bar hist for each variable
plt.xlabel('Value of situation')
plt.ylabel('Number of situation which lead into the value')
plt.title('Distribution of value with a size of {} cm'.format(Sizegrain))
plt.savefig('Grain_N150_S1p0_B3p0_Figure1V2.jpeg')

plt.figure(2)
Ener34 = len((list(filter(lambda a : 3 < a < 4, Result))))                  #A variable in which we will count the number between 3 and 4
Ener45 = len((list(filter(lambda a : 4 < a < 5, Result))))                  #A variable in which we will count the number between 4 and 5
Ener56 = len((list(filter(lambda a : 5 < a < 6, Result))))                  #A variable in which we will count the number between 5 and 6
Ener67 = len((list(filter(lambda a : 6 < a < 7, Result))))                  #A variable in which we will count the number between 6 and 7
Ener78 = len((list(filter(lambda a : 7 < a < 8, Result))))                  #A variable in which we will count the number between 7 and 8
Ener89 = len((list(filter(lambda a : 8 < a < 9, Result))))                  #A variable in which we will count the number between 8 and 9
Ener91 = len((list(filter(lambda a : 9 < a < 10, Result))))                 #A variable in which we will count the number between 9 and 10
Ener11 = len((list(filter(lambda a : 10 < a < 11, Result))))                #A variable in which we will count the number between 10 and 11

names = ['3-4', '4-5', '5-6','6-7','7-8','8-9','9-10','10-11']              #x_axis of the bar plot
Values = [Ener34,Ener45,Ener56,Ener67,Ener78,Ener89,Ener91,Ener11] 

plt.bar(names,Values)
plt.xlabel('Value of energy in eV')
plt.ylabel('Number of Electron in this energy')                             #Plot a bar hist for each variable
plt.title('Distribution of photo-electron for the grain with a size of {} cm'.format(Sizegrain))
plt.savefig('Grain_N150_S1p0_B3p0_Figure2V2.jpeg')

plt.show()