#!/usr/bin/python3

import numpy as np
import matplotlib.pylab as np
import numpy.random as rand
import numpy.fft as fft

def addGRF(cube, sigma_dens, Adens, beta, Npad, Kmin, beta_in, smooth=None):

   ### Get shape and size of the cube
   dims = np.shape(cube)
   nAxes = len(dims)   # number of axes


   ### Generate a GRF cube

   # Create a padded cube in Fourier space
   Cubedims = np.zeros(nAxes, np.int32)
   for i in range(nAxes):
      Cubedims[i] = dims[i] + Npad

   FourierCube = np.ones(Cubedims, complex)

   # Create cubes of Fourier coordinates
   centre = Cubedims/2
   Coords = np.indices(Cubedims, complex)
   for i in range(nAxes): Coords[i] -= centre[i]
   Kradius = np.sqrt(np.sum(Coords**2,axis=0))

   # Fill the Fourier cube with the requiered power law(s)
   FourierCube = Kradius**(-beta*0.5)
   m_in = np.nonzero(Kradius<Kmin)
   FourierCube[m_in] = (Kmin**(-beta*0.5) / Kmin**4) * Kradius[m_in]**4

   # Fix NaNs
   FourierCube[np.nonzero(FourierCube!=FourierCube)] = 0.

   # Randomize the phases of the Fourier cube
   Phase = np.ones(Cubedims, complex)
   Phase = rand.seed()   # necessary to have different random number in parallel threads (if forked at the same time, they get the same seed unless np.random.seed() is called)
   Phase = rand.uniform(size=Cubedims)*2*np.pi*1j
   Phase *= np.exp(Phase)
   FourierCube *= Phase

   # Multiply by a smoothing Gaussian
   if (smooth):
      Ksmooth = float(dims[0])/smooth
      FourierCube *= np.exp(-(Kradius)**2/(2*Ksmooth**2))

   # Compute the density cube by inverse Fourier transform the Fourier cube
   RealCube = fft.fftn(fft.ifftshift(FourierCube))

   Density = np.ones(dims, np.float32)
   Indices = []
   for i in range(nAxes):
      Indices.append(slice(Npad//2,Cubedims[i]-Npad//2-(Npad%2)))
   Indices = tuple(Indices)

   Density = np.exp(sigma_dens*np.real(RealCube[Indices]))
   Density /= np.mean(Density.flatten())

   return Density * Adens


def addGRF2(cube, sigma_dens, Adens, beta, Npad, Kmin, beta_in):

   ### Get shape and size of the cube
   dims = np.shape(cube)
   nAxes = len(dims)   # number of axes


   ### Generate a GRF cube

   # Create a padded cube in Fourier space
   Cubedims = np.zeros(nAxes, np.int32)
   for i in range(nAxes):
      Cubedims[i] = dims[i] + Npad

   FourierCube1 = np.ones(Cubedims, complex)
   FourierCube2 = np.ones(Cubedims, complex)

   # Create cubes of Fourier coordinates
   centre = Cubedims/2
   Coords = np.indices(Cubedims, complex)
   for i in range(nAxes): Coords[i] -= centre[i]
   Kradius = np.sqrt(np.sum(Coords**2,axis=0) )

   # Fill the Fourier cube with the requiered power law(s)
   FourierCube1 = np.exp( -beta*0.5 * np.log(Kradius) )
   FourierCube2 = np.exp( -beta*0.5 * np.log(Kradius) )
   m = np.nonzero(Kradius>10)
   FourierCube2[m] = np.exp( -beta*0.6 * np.log(Kradius[m]) )
   plt.loglog(Kradius.flatten(), FourierCube1.flatten(), '.b')
   plt.loglog(Kradius.flatten(), FourierCube2.flatten(), '.r')
   plt.show()
   m_in = np.nonzero(Kradius<Kmin)
#   FourierCube[m_in] = (Kmin**(-beta*0.5) / Kmin**4) * Kradius[m_in]**4
   FourierCube1[m_in] = 0.
   FourierCube2[m_in] = 0.

   # Fix NaNs
   FourierCube1[np.nonzero(FourierCube1!=FourierCube1)] = 0.
   FourierCube2[np.nonzero(FourierCube2!=FourierCube2)] = 0.

   # Randomize the phases of the Fourier cube
   Phase = np.ones(Cubedims, complex)
   Phase = np.random(Cubedims)*2*pi*1j
   Phase *= np.exp(Phase)
   FourierCube1 *= Phase
   FourierCube2 *= Phase

   # Compute the density cube by inverse Fourier transform the Fourier cube
   RealCube1 = fft.fftn(fft.ifftshift(FourierCube1))
   RealCube2 = fft.fftn(fft.ifftshift(FourierCube2))
   Density1 = np.ones(dims, np.float32)
   Density2 = np.ones(dims, np.float32)
   Indices = []
   for i in range(nAxes):
      Indices.append(slice(Npad//2,Cubedims[i]-Npad//2))
   Indices = tuple(Indices)

   Density1 = Adens * np.exp(sigma_dens*np.real(RealCube1[Indices]) )
   Density1 /= np.mean(Density1.flatten())
   Density2 = Adens * np.exp(sigma_dens*np.real(RealCube2[Indices]) )
   Density2 /= np.mean(Density2.flatten())

   return Density1, Density2




def addGRF_to_CRTfile(modelname, sigma_dens, Adens, beta, Npad, Kmin, beta_in):

   ### Load the Galaxy cube
   filename = '%s.crt' % (modelname)

   fp = file(filename, 'rb')

   nX, nY, nZ = np.fromfile(fp, np.int32, 3)
   print(nX, nY, nZ)

   cube = np.fromfile(fp, np.float32, nX*nY*nZ)
   Cube = cube.reshape((nZ, nY, nX))


   ### Generate a GRF cube

   # Create a padded cube in Fourier space
   Ncube1 = nZ + Npad   # 300 for N=256 (700 <=> ~26 Gbytes of RAM memory)
   Ncube2 = nY + Npad   # 300 for N=256 (700 <=> ~26 Gbytes of RAM memory)
   Ncube3 = nX + Npad   # 300 for N=256 (700 <=> ~26 Gbytes of RAM memory)
   Cubedims = np.asarray([Ncube1,Ncube2,Ncube3], np.int32)
   FourierCube = np.ones((Ncube1,Ncube2,Ncube3), complex)

   # Create cubes of Fourier coordinates
   centre=[Ncube1/2,Ncube2/2,Ncube3/2]
   X, Y, Z   = np.indices(Cubedims, complex)
   X        -= centre[0]
   Y        -= centre[1]
   Z        -= centre[2]
   Kradius = np.sqrt(X*X+Y*Y+Z*Z)

   # Fill the Fourier cube with the requiered power law(s)
   FourierCube = Kradius**(-beta*0.5)
   m_in = np.nonzero(Kradius<Kmin)
   FourierCube[m_in] = (Kmin**(-beta*0.5) / Kmin**4) * Kradius[m_in]**4

   # Fix NaNs
   FourierCube[nonzero(FourierCube!=FourierCube)] = 0.

   # Randomize the phases of the Fourier cube
   Phase = np.ones((Ncube1,Ncube2,Ncube3), complex)
   Phase = np.random(Cubedims)*2*pi*1j
   Phase *= np.exp(Phase)
   FourierCube *= Phase

   # Compute the density cube by inverse Fourier transform the Fourier cube
   RealCube = fft.fftn(fft.ifftshift(FourierCube))
   Density = np.ones((nZ,nY,nX), np.float32)

   npad = 0    # can be usefull to take only the inner part of the RealCube
   Density[npad:nZ-npad,npad:nY-npad,npad:nX-npad] = \
      np.exp( \
         sigma_dens*np.real(RealCube[Npad/2:Ncube1-Npad/2, \
                                    Npad/2:Ncube2-Npad/2, \
                                    Npad/2:Ncube3-Npad/2]) \
         + np.log(Adens) \
         )[npad:nZ-npad,npad:nY-npad,npad:nX-npad]

   Density /= np.mean(Density.flatten())

   # Use the GRF density cube to scale the input density cube
   Galaxy = Cube * Density

   # Plot coldens check
   if (0):
      coldens = np.ones((nY,nX))
      coldens = np.sum(Density, axis=0)
      coldens = coldens.flatten()
      plt.hist(np.log10(coldens))
      plt.show()

   # Drop Galaxy in a binary file for CRT
   if (1):
      GalaxyCube = real((Galaxy)).flatten()

      fp = file('%s.crt' % modelname, 'wb')
      dims = np.asarray([nX,nY,nZ], np.int32)
      dims.tofile(fp)
      GalaxyCube.tofile(fp)
      fp.close()
